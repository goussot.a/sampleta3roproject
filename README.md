# A3RO Template

This project is a template of the A3RO front-end stack used at SFEIR Factory.

Angular (Typescript) Rx Redux Redux-Observables

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Redux & Actions

When you create actions, add them to your root Actions type.
Same with your rootReducer and your rootEpic.

## EPICS

Epics are awesome.
Learn more here => https://redux-observable.js.org/docs/basics/Epics.html

## Testing Epics

We will use **marble testing** which will make our tests more concise and expressive.
We will have a **visual representation** of complex cases (error handling...).
We test the **consistency** of the system and the **behavior** of the circuit.

How to write nice marble tests => https://redux-observable.js.org/docs/recipes/WritingTests.html

NOTE: **cold** VS **hot** Observables -> https://medium.com/@benlesh/hot-vs-cold-observables-f8094ed53339

## Cypress
