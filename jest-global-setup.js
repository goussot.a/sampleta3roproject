module.exports = async () => {
  // set UTC (GMT) for timezone
  // this way NodeJs Date objects give correct values while running tests
  process.env.TZ = "UTC";
};
