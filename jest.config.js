module.exports = {
  verbose: true,
  transform: {
    ".(ts|tsx)": "ts-jest"
  },
  collectCoverage: true,
  globalSetup: "./jest-global-setup.js",
  preset: "ts-jest",
  coverageThreshold: {
    global: {
      statements: 75,
      functions: 60
    }
  },
  modulePathIgnorePatterns: ["cypress/"]
};
