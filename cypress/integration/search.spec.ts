describe("Verify search works properly", () => {
  it("display search after timeout of 1 sec", () => {
    cy.visit("/").then(() => {
      cy.get("input").type("test");
      setTimeout(
        () =>
          cy
            .get("div")
            .eq(2)
            .invoke("text")
            .should("eq", "test"),
        1000
      );
    });
  });

  it("don't display search immediatly", () => {
    cy.reload()
      .get("input")
      .type("test");
    cy.get("div")
      .eq(2)
      .invoke("text")
      .should("eq", `""`);
  });
});
