describe("Verify ajax request work properly", () => {
  it("Should get ajax data", () => {
    cy.visit("/");
    cy.server();
    cy.route({
      method: "GET",
      url: "http://www.mocky.io/v2/5e38257f3100005a00d37fed",
      response: { id: "2", value: "mocked server" }
    });
    cy.get("button")
      .eq(2)
      .click();
    cy.get("div")
      .eq(1)
      .invoke("text")
      .should("eq", `{\n  "id": "2",\n  "value": "mocked server"\n}`);
  });

  it("Should catch error and throw error", () => {
    cy.server();
    cy.route({
      method: "GET",
      url: "http://www.mocky.io/v2/5e3826b63100008d87d38001",
      status: 500,
      error: {}
    });
    cy.get("button")
      .eq(3)
      .click();
    cy.get("div")
      .eq(1)
      .contains(`"ajax error 500"`);
  });
});
