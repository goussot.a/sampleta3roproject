describe("Verify count works properly", () => {
  it("Should increment counter", () => {
    cy.visit("/");
    cy.get("button")
      .first()
      .click();
    cy.get("span")
      .invoke("text")
      .should("eq", "1");
  });

  it("Should decrement counter", () => {
    cy.get("button")
      .eq(1)
      .click();
    cy.get("span")
      .invoke("text")
      .should("eq", "0");
  });
});
