describe("Verify ajax request work properly", () => {
  it("Should get ajax data", () => {
    cy.visit("/");
    cy.server();
    cy.route("http://www.mocky.io/v2/5e38257f3100005a00d37fed").as("getMock");
    cy.get("button")
      .eq(2)
      .click();
    cy.wait("@getMock").then(xhr => {
      cy.get("div")
        .eq(1)
        .invoke("text")
        .should("eq", `{\n  "id": "2",\n  "value": "mock value"\n}`);
    });
  });

  it("Should catch error and throw error", () => {
    cy.server();
    cy.route("http://www.mocky.io/v2/5e3826b63100008d87d38001").as("getMock");
    cy.get("button")
      .eq(3)
      .click();
    cy.wait("@getMock").then(xhr => {
      cy.get("div")
        .eq(1)
        .contains(`"ajax error 500"`);
    });
  });
});
