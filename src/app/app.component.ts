import { NgRedux, select } from "@angular-redux/store";
import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { countActions } from "./actions/count.actions";
import { testRestActions } from "./actions/testRest.actions";
import { CountState } from "./store/count.reducer";
import { AppStore } from "./store/root.reducer";
import { TestRestState } from "./store/testRest.reducer";
import { searchActions } from "./actions/search.action";
import { SearchState } from "./store/search.reducer";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  styleUrls: ["./app.component.css"],
  template: `
    <div>
      <input (keyup)="onKey($event)" />
      <span>{{ counterValue$ | async }}</span>
      <button (click)="increment()">+1</button>
      <button (click)="decrement()">-1</button>
      <button (click)="testRest()">Test a REST call</button>
      <button (click)="testErrorRest()">Test a REST Call with error</button>
      <a [routerLink]="'/test'">child module</a>
      <div>{{ testRestValue$ | async | json }}</div>
      <div>{{ inputValue$ | async | json }}</div>
      <div [innerHTML]="'TEST' | translate"></div>
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent implements OnInit {
  @select(["count"]) counterValue$: Observable<CountState>;
  @select(["restData"]) testRestValue$: Observable<TestRestState>;
  @select(["search"]) inputValue$: Observable<SearchState>;

  public flag = true;

  constructor(
    private ngRedux: NgRedux<AppStore>,
    private translate: TranslateService
  ) {
    this.translate.use("fr");
  }

  ngOnInit() {}

  onKey(event: any) {
    this.ngRedux.dispatch(searchActions.inputChanged(event.target.value));
  }

  testRest() {
    this.ngRedux.dispatch(testRestActions.load());
  }

  testErrorRest() {
    this.ngRedux.dispatch(testRestActions.loadError());
  }

  increment() {
    this.ngRedux.dispatch(countActions.increment());
  }

  decrement() {
    this.ngRedux.dispatch(countActions.decrement());
  }
}
