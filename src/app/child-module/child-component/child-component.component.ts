import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-child-component",
  styleUrls: ["./child-component.component.css"],
  template: `
    <p>{{ "CHILD_TEST" | translate }}</p>
    <a routerLink="nth">nth-child</a>
  `
})
export class ChildComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
