import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule, Routes } from "@angular/router";
import { ChildComponent } from "./child-component/child-component.component";

const routes: Routes = [
  {
    path: "",
    component: ChildComponent
  },
  {
    path: "nth",
    loadChildren: "./nth-child/nth-child.module#NthChildModule"
  }
];

@NgModule({
  declarations: [ChildComponent],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ]
})
export class ChildModule {}
