import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule, Routes } from "@angular/router";
import { NthChildCComponent } from "./nth-child-c/nth-child-c.component";

const routes: Routes = [
  {
    path: "",
    component: NthChildCComponent
  }
];

@NgModule({
  declarations: [NthChildCComponent],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ]
})
export class NthChildModule {}
