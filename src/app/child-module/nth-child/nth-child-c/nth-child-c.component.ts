import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { CountState } from "src/app/store/count.reducer";
import { select } from "@angular-redux/store";

@Component({
  selector: "app-nth-child-c",
  styleUrls: ["./nth-child-c.component.css"],
  template: `
    <p>{{ "NTH_CHILD" | translate }}</p>
    <p [innerHTML]="counterValue$ | async | json"></p>
  `
})
export class NthChildCComponent implements OnInit {
  @select(["count"]) counterValue$: Observable<CountState>;

  constructor() {}

  ngOnInit() {}
}
