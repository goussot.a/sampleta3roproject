import { of, Observable } from "rxjs";
import { TranslateLoader } from "@ngx-translate/core";

export class CustomLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return of({ test: "value" });
  }
}
