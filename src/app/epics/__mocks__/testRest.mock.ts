export const testRestMockFailResponse = {
  response: new Error("Timeout fetching data")
};

export const testRestMockResponse = {
  response: {
    id: "2",
    value: "mock value"
  }
};
