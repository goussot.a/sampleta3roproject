import { TestScheduler } from "rxjs/testing";
import { testRestActions } from "../actions/testRest.actions";
import { testRestEpics, testRestEpic } from "./testRest.epic";
import { timer, throwError } from "rxjs";
import { mergeMap } from "rxjs/operators";
import {
  testRestMockFailResponse,
  testRestMockResponse
} from "./__mocks__/testRest.mock";

describe("testRestEpic", () => {
  let testScheduler: TestScheduler;

  beforeEach(() => {
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it("should signal fetching data has failed", () => {
    const marbles = {
      i: "-i", // input action
      r: "--|", // mock api response time duration
      o: "---o" // output action
    };

    const values = {
      i: testRestActions.load(),
      o: testRestActions.fail(testRestMockFailResponse)
    };

    testScheduler.run(({ hot, cold, expectObservable }) => {
      const action$ = hot(marbles.i, values) as any;
      const duration = testScheduler.createTime(marbles.r);
      const dependencies = {
        ajax: ({ url, method }: { url: string; method: string }) =>
          timer(duration).pipe(
            mergeMap(() => throwError(testRestMockFailResponse))
          )
      };
      const state$ = null as any;
      const output$ = testRestEpics(action$, state$, dependencies);
      expectObservable(output$).toBe(marbles.o, values);
    });
  });

  it("should fetch data and increment the counter", () => {
    const marbles = {
      i: "-i", // input action
      r: "--r", // mock api response
      o: "---o" // output action
    };

    const values = {
      i: testRestActions.load(),
      r: testRestMockResponse,
      o: testRestActions.success(testRestMockResponse.response)
    };

    testScheduler.run(({ hot, cold, expectObservable }) => {
      const action$ = hot(marbles.i, values) as any;
      const dependencies = {
        ajax: ({ url, method }: { url: string; method: string }) =>
          cold(marbles.r, values)
      };
      const state$ = null as any;
      const output$ = testRestEpic(action$, state$, dependencies);
      expectObservable(output$).toBe(marbles.o, values);
    });
  });
});
