import { TestScheduler } from "rxjs/testing";
import { searchEpics } from "./search.epic";
import { searchActions } from "../actions/search.action";

describe("SearchEpic", () => {
  let testScheduler: TestScheduler;

  beforeEach(() => {
    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it("should trigger loading once the user stop typing", () => {
    const marbles = {
      i: "-ab--------", // input action
      r: "--t--------", // api response
      o: "-- 1s o" // output action
    };

    const values = {
      a: searchActions.inputChanged("test1"),
      b: searchActions.inputChanged("test2"),
      o: searchActions.load("test2"),
      t: "test"
    };

    testScheduler.run(({ hot, cold, expectObservable }) => {
      const action$ = hot(marbles.i, values) as any;
      const dependencies = {
        ajax: ({ url, method }: { url: string; method: string }) =>
          cold(marbles.r, values)
      };
      const state$ = null as any;
      const output$ = searchEpics(action$, state$, dependencies);
      expectObservable(output$).toBe(marbles.o, values);
    });
  });

  it("should load the corresponding inputs", () => {
    const marbles = {
      i: "-ab", // input action
      r: "---t", // api response
      o: "-----o" // output action
    };

    const values = {
      a: searchActions.load("test1"),
      b: searchActions.load("test2"),
      o: searchActions.success("test2"),
      t: "test"
    };

    testScheduler.run(({ hot, cold, expectObservable }) => {
      const action$ = hot(marbles.i, values) as any;
      const dependencies = {
        ajax: ({ url, method }: { url: string; method: string }) =>
          cold(marbles.r, values)
      };
      const state$ = null as any;
      const output$ = searchEpics(action$, state$, dependencies);
      expectObservable(output$).toBe(marbles.o, values);
    });
  });
});
