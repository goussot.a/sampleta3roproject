import { Observable, timer, of } from "rxjs";
import {
  mergeMap,
  switchMap,
  concatMap,
  catchError,
  debounceTime
} from "rxjs/operators";
import {
  SearchActions,
  SearchTypes,
  searchActions
} from "../actions/search.action";
import { Epic, ofType, combineEpics } from "redux-observable";
import { SearchState } from "../store/search.reducer";
import { dependencies } from "../app.store";
import { AjaxResponse, AjaxError } from "rxjs/ajax";
import { URLS } from '../const/urls';

export const inputEpic: Epic = (
  action$: Observable<SearchActions>,
  state$: Observable<SearchState>,
  { ajax }: typeof dependencies
): Observable<SearchActions> => {
  return action$.pipe(
    ofType(SearchTypes.InputChanged),
    debounceTime(1000),
    switchMap((action: any) => of(searchActions.load(action.input)))
  );
};

export const searchEpic: Epic = (
  action$: Observable<SearchActions>,
  state$: Observable<SearchState>,
  { ajax }: typeof dependencies
): Observable<SearchActions> => {
  return action$.pipe(
    ofType(SearchTypes.Load),
    switchMap((action: any) => {
      return ajax({ url: URLS.search, method: "GET" }).pipe(
        concatMap((ajaxRes: AjaxResponse) => {
          return of(searchActions.success(action.input));
        }),
        catchError((err: AjaxError) => {
          return of(searchActions.fail(err));
        })
      );
    })
  );
};

export const searchEpics = combineEpics(searchEpic, inputEpic);
