import { Observable, of } from "rxjs";
import { AjaxError, AjaxResponse } from "rxjs/ajax";
import { catchError, concatMap } from "rxjs/operators";
import {
  TestRestActions,
  testRestActions,
  TestRestTypes
} from "../actions/testRest.actions";
import { combineEpics, Epic, ofType } from "redux-observable";
import { URLS } from "../const/urls";
import { TestRestState } from "../store/testRest.reducer";
import { dependencies } from "../app.store";

export const testRestEpic: Epic = (
  action$: Observable<TestRestActions>,
  state$: Observable<TestRestState>,
  { ajax }: typeof dependencies
): Observable<TestRestActions> => {
  return action$.pipe(
    ofType(TestRestTypes.Load),
    concatMap(() => {
      return ajax({ url: URLS.success, method: "GET" }).pipe(
        concatMap((ajaxRes: AjaxResponse) => {
          return of(testRestActions.success(ajaxRes.response));
        }),
        catchError((err: AjaxError) => {
          return of(testRestActions.fail(err));
        })
      );
    })
  );
};

export const testRestErrorEpic: Epic = (
  action$: Observable<TestRestActions>,
  state$: Observable<TestRestState>,
  { ajax }: typeof dependencies
): Observable<TestRestActions> =>
  action$.pipe(
    ofType(TestRestTypes.LoadWithError),
    concatMap(action => {
      return ajax({ url: URLS.fail, method: "GET" }).pipe(
        concatMap((ajaxRes: AjaxResponse) => {
          return of(testRestActions.success(ajaxRes.response));
        }),
        catchError((err: AjaxError) => {
          return of(testRestActions.fail(err));
        })
      );
    })
  );

export const testRestEpics = combineEpics(testRestEpic, testRestErrorEpic);
