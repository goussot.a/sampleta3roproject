import { testRestEpics } from "./testRest.epic";
import { searchEpics } from "./search.epic";
import { combineEpics } from "redux-observable";

export const rootEpic = combineEpics(testRestEpics, searchEpics);
