import { NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { AnyAction } from "redux";
import { getService } from "./service-injector.utils";

export const ACNavigate = (path: string) =>
  ({ type: "@router/navigate", path } as const);

export type NavigateAction = ReturnType<typeof ACNavigate>;

// Next function is a TypeGuard
function isNavigate(
  action: AnyAction | NavigateAction
): action is NavigateAction {
  return (action as NavigateAction).path !== undefined;
}

export const routerMiddleware = store => next => action => {
  if (isNavigate(action)) {
    const router: Router = getService(Router);
    const ngZone: NgZone = getService(NgZone);

    ngZone.run(() => {
      router.navigate([action.path]);
    });
  }
  return next(action);
};
