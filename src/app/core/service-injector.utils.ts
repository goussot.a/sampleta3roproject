import { Injector } from "@angular/core";

let AppInjector: Injector = null;

export function loadInjector(injector?: Injector) {
  AppInjector = injector;
}

export function getService(serviceClass: any) {
  if (AppInjector) {
    return AppInjector.get(serviceClass);
  }
}
