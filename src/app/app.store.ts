import { applyMiddleware, compose, createStore, Store } from "redux";
import { createLogger } from "redux-logger";
import { routerMiddleware } from "./core/router.middleware";
import { rootEpic } from "./epics/root.epic";
import { AppStore, rootReducer } from "./store/root.reducer";
import { createEpicMiddleware } from "redux-observable";
import { ajax } from "rxjs/ajax";

export const dependencies = { ajax } as const;

const epicMiddleware = createEpicMiddleware({
  dependencies
});

const middlewares = [routerMiddleware, epicMiddleware, createLogger()];
const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store: Store<AppStore> = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...middlewares))
);

epicMiddleware.run(rootEpic);
