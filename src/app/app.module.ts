import { NgRedux, NgReduxModule } from "@angular-redux/store";
import { BrowserModule } from "@angular/platform-browser";
import { Injector, NgModule } from "@angular/core";
import { store } from "./app.store";
import { loadInjector } from "./core/service-injector.utils";
import { AppRoutingModule } from "./router/app-routing.module";
import { AppComponent } from "./app.component";
import { AppStore } from "./store/root.reducer";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { registerLocaleData } from "@angular/common";
import localeFr from "@angular/common/locales/fr";
import { RouterModule, Routes } from "@angular/router";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const routes: Routes = [
  {
    path: "test",
    loadChildren: "./child-module/child-module.module#ChildModule"
  }
];

registerLocaleData(localeFr, "fr");
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgReduxModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector, ngRedux: NgRedux<AppStore>) {
    ngRedux.provideStore(store);
    loadInjector(injector);
  }
}
