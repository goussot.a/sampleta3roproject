import { ActionTypes } from "../core/actions.type";

export enum CountTypes {
  Increment = "@Count/Increment",
  Decrement = "@Count/Decrement"
}

export const countActions = {
  increment: () => ({ type: CountTypes.Increment } as const),
  decrement: () => ({ type: CountTypes.Decrement } as const)
};
export type CountActions = ActionTypes<typeof countActions>;
