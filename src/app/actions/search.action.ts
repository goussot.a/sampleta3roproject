import { ActionTypes } from "../core/actions.type";

export enum SearchTypes {
  Load = "@Search/load",
  Fail = "@Search/fail",
  InputChanged = "@Search/input_changed",
  Success = "@Search/success"
}

export const searchActions = {
  inputChanged: (input: string) =>
    ({ type: SearchTypes.InputChanged, input } as const),
  load: (input: string) => ({ type: SearchTypes.Load, input } as const),
  success: data => ({ type: SearchTypes.Success, data } as const),
  fail: error => ({ type: SearchTypes.Fail, error } as const)
};
export type SearchActions = ActionTypes<typeof searchActions>;
