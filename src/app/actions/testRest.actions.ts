import { ActionTypes } from "../core/actions.type";

export enum TestRestTypes {
  Load = "@TestRest/load",
  LoadWithError = "@TestRest/loadWithError",
  Success = "@TestRest/success",
  Fail = "@TestRest/fail",
  Search = "@TestRest/search"
}

export const testRestActions = {
  load: () => ({ type: TestRestTypes.Load } as const),
  search: () => ({ type: TestRestTypes.Search } as const),
  loadError: () => ({ type: TestRestTypes.LoadWithError } as const),
  success: data => ({ type: TestRestTypes.Success, data } as const),
  fail: error => ({ type: TestRestTypes.Fail, error } as const)
};
export type TestRestActions = ActionTypes<typeof testRestActions>;
