import { CountActions } from "./count.actions";
import { TestRestActions } from "./testRest.actions";
import { SearchActions } from "./search.action";

export type Actions = CountActions | TestRestActions | SearchActions;
