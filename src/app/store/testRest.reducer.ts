import { TestRestTypes } from "../actions/testRest.actions";

interface RestResponse {
  id?: number;
  value?: string;
}

export type TestRestState = Readonly<any>;

export const testRestReducer = (
  testRestState: RestResponse = { id: null, value: "default" },
  action: TestRestState
): RestResponse => {
  switch (action.type) {
    case TestRestTypes.Success:
      return action.data;
    case TestRestTypes.Fail:
      return action.error;
    default:
      return testRestState;
  }
};
