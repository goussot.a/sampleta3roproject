import { testRestActions } from "../actions/testRest.actions";
import { testRestReducer, TestRestState } from "./testRest.reducer";
import {
  testRestMockResponse,
  testRestMockFailResponse
} from "../epics/__mocks__/testRest.mock";

const defaultState: TestRestState = { id: "1", value: "" };

describe("TestRest Reducer", () => {
  it("should return initial state", () => {
    const action = { type: "NO_ACTION" };
    const newState = testRestReducer(defaultState, action as any);
    expect(newState).toEqual(defaultState);
  });

  it("should return incremented counter", () => {
    const newState = testRestReducer(
      defaultState,
      testRestActions.success(testRestMockResponse)
    );
    expect(newState).toEqual(testRestMockResponse);
  });

  it("should return decremented counter", () => {
    const newState = testRestReducer(
      defaultState,
      testRestActions.fail(testRestMockFailResponse)
    );
    expect(newState).toEqual(testRestMockFailResponse);
  });
});
