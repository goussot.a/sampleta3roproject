import { combineReducers, Reducer } from "redux";
import { countReducer } from "./count.reducer";
import { testRestReducer } from "./testRest.reducer";
import { searchReducer } from "./search.reducer";

export interface AppStore {
  count: ReturnType<typeof countReducer>;
  restData: ReturnType<typeof testRestReducer>;
  search: ReturnType<typeof searchReducer>;
}

export const rootReducer: Reducer<AppStore> = combineReducers({
  count: countReducer,
  restData: testRestReducer,
  search: searchReducer
});
