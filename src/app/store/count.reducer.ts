import { CountActions, CountTypes } from "../actions/count.actions";

export type CountState = Readonly<number>;

export const countReducer = (
  countState: CountState = 0,
  action: CountActions
): number => {
  switch (action.type) {
    case CountTypes.Increment:
      return countState + 1;
    case CountTypes.Decrement:
      return countState - 1;
    default:
      return countState;
  }
};
