import { SearchTypes } from "../actions/search.action";

export type SearchState = Readonly<any>;

export const searchReducer = (
  searchState: string = "",
  action: SearchState
): string => {
  switch (action.type) {
    case SearchTypes.Success:
      return action.data;
    case SearchTypes.Fail:
      return action.error;
    default:
      return searchState;
  }
};
