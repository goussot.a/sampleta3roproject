import { countActions } from "../actions/count.actions";
import { countReducer, CountState } from "./count.reducer";

const defaultState: CountState = 0;

describe("Count Reducer", () => {
  it("should return initial state", () => {
    const action = { type: "NO_ACTION" };
    const newState = countReducer(defaultState, action as any);
    expect(newState).toEqual(defaultState);
  });

  it("should return incremented counter", () => {
    const newState = countReducer(defaultState, countActions.increment());
    expect(newState).toEqual(defaultState + 1);
  });

  it("should return decremented counter", () => {
    const newState = countReducer(defaultState, countActions.decrement());
    expect(newState).toEqual(defaultState - 1);
  });
});
